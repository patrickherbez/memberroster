﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DddsUtils.Text.Export;
using DddsUtils.Text.Export.Delimiters;

namespace Innovaccer.Member.Roster
{
    class Program
    {
        static void Main(string[] args)
        {

            var repo = new Innovaccer.Member.Roster.Repository.MemberRosterRepository();

            Console.WriteLine("Getting Roster");
            var list = repo.RetrieveMemberRoster();

            Console.WriteLine("Creating file");
            var file = new File();
            var rows = file.Create(list, Delimiter.Pipe, @"C:\Users\patrick.herbez\Documents\TessellateMemberRoster.psf", true).ToList();

            Console.WriteLine("File created");
            Console.ReadKey();
        }
    }
}
