﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovaccer.Member.Roster.Models
{
    public class Roster
    {
        public string Empi { get; set; }

        public string Fn { get; set; }

        public string Ln { get; set; }

        public DateTime DoB { get; set; }

        public string Gn { get; set; }

        public string Id { get; set; }

        public string Idt { get; set; }

        public string Mn { get; set; }

        public string Tel1 { get; set; }

        public string Psa1 { get; set; }

        public string Psa2 { get; set; }

        public string Pz { get; set; }
    }
}
