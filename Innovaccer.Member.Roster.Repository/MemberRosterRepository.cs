﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DddsUtils.Data.Context;
using NLog;
using LogFactory = DddsUtils.Logging.LogFactory;
using System.Configuration;
using System.Data;

namespace Innovaccer.Member.Roster.Repository
{
    public class MemberRosterRepository
    {
        private ILogger _logger;
        private IDapperContext _ctx;
        public MemberRosterRepository()
        {
            var lf = new LogFactory();
            _logger = lf.GetLogger("MemberRosterLogger");

            _ctx = new DapperContext(new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString));
            _ctx.Logger = _logger;
        }

        public IEnumerable<Models.Roster> RetrieveMemberRoster()
        {
            _logger.Info("Into RetrieveMemberRoster");

            var sql = @"SELECT 
	                        distinct
	                        mm.MemberID empi,
	                        mm.FirstName fn,
	                        mm.LastName ln,
	                        mm.DateOfBirth dob,
	                        mm.Gender gn,
	                        mm.MemberId id,
	                        'Patient ID' idt,
	                        mm.MiddleInitial mn,
	                        mm.[Phone] tel1,
	                        mm.[AddressLine1] psa1,
	                        mm.[AddressLine2] psa2,
	                        mm.[ZipCode] pz
                        FROM [MerlinRA7BCBSM].[dbo].[tblMemberMaster] mm
                                    left join (Select MemberMaster_WK, max(EligibleMonth) as EligibleMonth from [MerlinRA7BCBSM].[dbo].tblCacheCMSEligibility cm group by MemberMaster_WK) mx on mm.MemberMaster_WK = mx.MemberMaster_WK
                                                join [MerlinRA7BCBSM].[dbo].tblCacheCMSEligibility cms on mx.MemberMaster_WK = cms.MemberMaster_WK
                                                and mx.EligibleMonth = cms.EligibleMonth
                              and cms.EligibilityEd >= '12/31/2017'  
                                    left join [MerlinRA7BCBSM].[dbo].tblPlans p on cms.Plan_WK = p.Plan_WK
                                    left join [MerlinRA7BCBSM].[dbo].tblSpanMemberEligibility sp on mm.MemberMaster_WK = sp.MemberMaster_WK
                                                and sp.EFF = 1
                                                and sp.IsActive = 1
                          WHERE mm.[Level4_WK] in (
                           SELECT [Level4_WK]
                            --,[Level4_ID]
                            --,[Level4_Name]
                            --,[Level4_DisplayName]
                           FROM [MerlinRA7BCBSM].[dbo].[tblLevel4]
                           WHERE  [Level4_ID] in (
                            SELECT [practicecode]    
                           FROM [WorkBiBCBSM].[dbo].[templx_practice]
                           WHERE [OrgCode] in ('ihp','gmp','thirlby','westfront','arcturus') ) 
                           )";

            var result = _ctx.Query<Models.Roster>(sql, null, CommandType.Text, 120, null);

            _logger.Info("Completed RetrieveMemberRoster");

            return result;
        }
    }
}
